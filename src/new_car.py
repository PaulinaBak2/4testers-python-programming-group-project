from datetime import date


class Car:
    def __init__(self, model, production_year):
        self.model = model
        self.production_year = production_year
        self.course = 0

    def increase_course_with_driven_distance(self, distance):
        self.course += distance

    def check_if_car_has_warranty(self):
        year = date.today().year
        age_of_a_car = year - self.production_year
        if age_of_a_car > 7 or self.course > 120_000:
            return False
        else:
            return True

    def get_car_description(self):
        return f'This is a {self.model} made in {self.production_year}. Currently it drove {self.course} kilometers'


if __name__ == '__main__':
    car1 = Car("Honda Civic", 1998)
    car2 = Car("Toyota Yaris", 2020)
    print(car1.model)
    print(car1.production_year)
    print(car1.course)
    car1.increase_course_with_driven_distance(200000)
    car2.increase_course_with_driven_distance(20000)
    print(car1.course)
    print(car2.course)
    car1.get_car_description()
    car2.get_car_description()
    print(car1.check_if_car_has_warranty())
    print(car2.check_if_car_has_warranty())
    car3 = Car("Skoda Fabia", 2020)
    car3.increase_course_with_driven_distance(119000)
    print(car3.check_if_car_has_warranty())
